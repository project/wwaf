<?php

/**
 * Implements hook_token_info().
 */
function wwaf_token_info() {
  $type = [
    'name' => t('WWAFs'),
    'description' => t('Tokens related to individual Where we are finder points.'),
    'needs-data' => 'wwaf',
  ];

  $wwaf['id'] = [
    'name' => t("WWAF ID"),
    'description' => t("The unique ID of the node."),
    'type' => 'wwaf',
  ];

  $wwaf['name'] = [
    'name' => t("Name of the point"),
    'type' => 'wwaf',
  ];

  return [
    'types' => ['wwaf' => $type],
    'tokens' => ['wwaf' => $wwaf],
  ];
}

/**
 * Implements hook_tokens().
 */
function wwaf_tokens($type, $tokens, array $data, array $options, \Drupal\Core\Render\BubbleableMetadata $bubbleable_metadata) {
  $ts = \Drupal::token();

  $replacements = [];
  if ($type == 'wwaf' && !empty($data['wwaf'])) {
    /** @var \Drupal\wwaf\Entity\WwafInterface $wwaf */
    $wwaf = $data['wwaf'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'id':
          $replacements[$original] = $wwaf->id();
          break;

        case 'name':
          $replacements[$original] = $wwaf->label();
      }
    }
  }

  return $replacements;
}
