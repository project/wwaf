<?php

namespace Drupal\wwaf;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Where We Are Finder entities.
 *
 * @ingroup wwaf
 */
class WwafListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Where We Are Finder ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\wwaf\Entity\Wwaf $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.wwaf.edit_form',
      ['wwaf' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
