<?php

namespace Drupal\wwaf\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Where We Are Finder entity.
 *
 * @ingroup wwaf
 *
 * @ContentEntityType(
 *   id = "wwaf",
 *   label = @Translation("Where We Are Finder"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\wwaf\WwafListBuilder",
 *     "views_data" = "Drupal\wwaf\Entity\WwafViewsData",
 *     "translation" = "Drupal\wwaf\WwafTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\wwaf\Form\WwafForm",
 *       "add" = "Drupal\wwaf\Form\WwafForm",
 *       "edit" = "Drupal\wwaf\Form\WwafForm",
 *       "update" = "Drupal\wwaf\Form\WwafForm",
 *       "delete" = "Drupal\wwaf\Form\WwafDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\wwaf\WwafHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\wwaf\WwafAccessControlHandler",
 *   },
 *   base_table = "wwaf",
 *   data_table = "wwaf_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer wwaf entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/wwaf/{wwaf}",
 *     "add-form" = "/wwaf/add",
 *     "edit-form" = "/wwaf/{wwaf}/edit",
 *     "delete-form" = "/wwaf/{wwaf}/delete",
 *     "collection" = "/admin/wwaf/list",
 *   },
 *   field_ui_base_route = "wwaf.configuration.structure"
 * )
 */
class Wwaf extends ContentEntityBase implements WwafInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    //---------------------------------
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    // Add the user field.
    //---------------------------------
    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Where We Are Finder entity.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)

      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])

      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
    ;

    // Main name / label.
    //---------------------------------
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Where We Are Finder entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)

      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])

      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
    ;

    // Published / un-published.
    //---------------------------------
    $fields['status']->setDescription(t('A boolean indicating whether the Where We Are Finder is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 100,
      ])
    ;

    // Created.
    //---------------------------------
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'))
    ;

    // Updated (changed).
    //---------------------------------
    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'))
    ;

    // Main geolocation field.
    //---------------------------------
    $fields['gps'] = BaseFieldDefinition::create('geolocation')
      ->setLabel(t('GPS Coordinates'))
      ->setDescription(t('The GPS Coordinates of this Destination. You may enter address in the search field and the location will be retrieved via Google Maps.'))
      ->setRequired(TRUE)

      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', array(
        'type' => 'geolocation_googlegeocoder',
        'weight' => -2,
        'title' => t('This is a test title'),
        'settings' => array(
          'set_marker' => '1',
          'info_text' => t('Some info text'),
          'use_overridden_map_settings' => 0,
          'google_map_settings' => array(
            'type' => 'TERRAIN',
            'zoom' => 5,
            'mapTypeControl' => TRUE,
            'streetViewControl' => FALSE,
            'zoomControl' => TRUE,
            'scrollwheel' => FALSE,
            'disableDoubleClickZoom' => FALSE,
            'draggable' => TRUE,
            'height' => '450px',
            'width' => '100%',
            'info_auto_display' => TRUE,
            'disableAutoPan' => TRUE,
            'preferScrollingToZooming' => FALSE,
            'gestureHandling' => 'auto',
          ),

          // Disabled till the issue "https://www.drupal.org/project/geolocation/issues/2936722" will be fixed
          // 'populate_address_field' => TRUE,       // Enables the AJAX POPULATION of the address field
          // 'target_address_field' => 'address',    // machine_name of the field that must be populated

          'default_longitude' => 11.9000639,
          'default_latitude'  => 45.510844,
          'auto_client_location' => FALSE,
          'auto_client_location_marker' => FALSE,
          'allow_override_map_settings' => FALSE,
        ),
      ))

      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'geolocation_map',
        'weight' => -2,
        'settings' => array(
          'set_marker' => '1',
          'info_text' => 'lat,long: :lat,:lng',
          'google_map_settings' => array(
            'type' => 'TERRAIN',
            'zoom' => 9,
            'mapTypeControl' => TRUE,
            'streetViewControl' => FALSE,
            'zoomControl' => TRUE,
            'scrollwheel' => FALSE,
            'disableDoubleClickZoom' => FALSE,
            'draggable' => TRUE,
            'height' => '300px',
            'width' => '100%',
            'info_auto_display' => TRUE,
            'disableAutoPan' => TRUE,
            'preferScrollingToZooming' => FALSE,
            'gestureHandling' => 'auto',
          ),
        ),
      ))
    ;

    // Type of the WWAF Point (taxonomy_term reference).
    //---------------------------------
    $fields['type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Point type'))
      ->setDescription(t('The type of this point.'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default')
      ->setSetting('handler_settings', [
        'target_bundles' => [
          'wwaf_point_types'
        ]
      ])

      ->setCardinality(1)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', array(
        'type' => 'select',
        'weight' => -2,
        'title' => t('This is a test title'),
        'settings' => array(
          'set_marker' => '1',
          'info_text' => t('Some info text'),
          'use_overridden_map_settings' => 0,
          'google_map_settings' => array(
            'type' => 'TERRAIN',
            'zoom' => 5,
            'mapTypeControl' => TRUE,
            'streetViewControl' => FALSE,
            'zoomControl' => TRUE,
            'scrollwheel' => FALSE,
            'disableDoubleClickZoom' => FALSE,
            'draggable' => TRUE,
            'height' => '450px',
            'width' => '100%',
            'info_auto_display' => TRUE,
            'disableAutoPan' => TRUE,
            'preferScrollingToZooming' => FALSE,
            'gestureHandling' => 'auto',
          ),

          // Disabled till the issue "https://www.drupal.org/project/geolocation/issues/2936722" will be fixed
          // 'populate_address_field' => TRUE,       // Enables the AJAX POPULATION of the address field
          // 'target_address_field' => 'address',    // machine_name of the field that must be populated

          'default_longitude' => 11.9000639,
          'default_latitude'  => 45.510844,
          'auto_client_location' => FALSE,
          'auto_client_location_marker' => FALSE,
          'allow_override_map_settings' => FALSE,
        ),
      ))

      ->setDisplayConfigurable('view', TRUE)
    ;

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function toDataArray() {
    // TODO: Implement toDataArray() method.
  }

}
