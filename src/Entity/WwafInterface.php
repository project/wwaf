<?php

namespace Drupal\wwaf\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Where We Are Finder entities.
 *
 * @ingroup wwaf
 */
interface WwafInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Where We Are Finder name.
   *
   * @return string
   *   Name of the Where We Are Finder.
   */
  public function getName();

  /**
   * Sets the Where We Are Finder name.
   *
   * @param string $name
   *   The Where We Are Finder name.
   *
   * @return \Drupal\wwaf\Entity\WwafInterface
   *   The called Where We Are Finder entity.
   */
  public function setName($name);

  /**
   * Gets the Where We Are Finder creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Where We Are Finder.
   */
  public function getCreatedTime();

  /**
   * Sets the Where We Are Finder creation timestamp.
   *
   * @param int $timestamp
   *   The Where We Are Finder creation timestamp.
   *
   * @return \Drupal\wwaf\Entity\WwafInterface
   *   The called Where We Are Finder entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Converts the Entity into JSONSerializable output.
   *
   * @return mixed
   */
  public function toDataArray();

}
