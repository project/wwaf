<?php

namespace Drupal\wwaf;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Where We Are Finder entity.
 *
 * @see \Drupal\wwaf\Entity\Wwaf.
 */
class WwafAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\wwaf\Entity\WwafInterface $entity */

    switch ($operation) {

      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished wwaf entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published wwaf entities');

      case 'edit':
      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit wwaf entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete wwaf entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add wwaf entities');
  }


}
