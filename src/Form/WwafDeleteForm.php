<?php

namespace Drupal\wwaf\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Where We Are Finder entities.
 *
 * @ingroup wwaf
 */
class WwafDeleteForm extends ContentEntityDeleteForm {


}
