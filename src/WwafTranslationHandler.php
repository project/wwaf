<?php

namespace Drupal\wwaf;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for wwaf.
 */
class WwafTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
